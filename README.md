# mfcoind-data

MFCoind latest data for fast initial sync.

# Limitation

As first attempt to commit all changes of MFCoin blockchain to BitBucket failed due to repository size growing too fast,
the idea was changed to store latest archive of the data on Dropbox and just refresh the archive every hour or so.

# URLs

* Public URL with all files: https://www.dropbox.com/sh/4smw4sv7eouap51/AADBupmynU4lrzHVWXDtkSHfa?dl=0
* Public URL to the latest archive directly: https://www.dropbox.com/s/f6a3465zudvltz9/latest.tar.gz?dl=0

# Usage

Assuming MFCoind is installed but not initialized (never run)


```
cd ~
wget https://www.dropbox.com/s/f6a3465zudvltz9/latest.tar.gz?dl=1 -O latest.tar.gz
tar -xzf latest.tar.gz
rm -f latest.tar.gz
echo "rpcuser=MFCoinrpc" >> .MFCoin/MFCoin.conf
echo "prcpassword=MFCoinrpcPass" >> .MFCoin/MFCoin.conf
MFCoind -daemon -index
```

# Notes

In the public directory of BitBucket you can also find the `latest.txt` with latest block number in archive
as well as `sync.log` to see how it was changed
